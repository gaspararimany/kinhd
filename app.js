var express = require('express');
var excelToJson = require('convert-excel-to-json');
var dir = require('node-dir');
var app = express();

var port = process.env.PORT || 5000;
var xlsNames;

var nav = [
    {
        Text: 'Books',
        Link: '/Books'
    },
    {
        Text: 'Authors',
        Link: '/Authors'
    },
    {
        Text: 'Series',
        Link: '/Series'
    }];

//app.use(express.static(__dirname + '/src/views'));
app.use(express.static('src/views'));
app.use(express.static('xls/inputs'));
app.use(express.static('public'));

app.set('views', __dirname + '/views');

var xlsNames =
dir.files(__dirname+'/xls/inputs',{sync:true});

console.log(xlsNames);

var xslAsJson = excelToJson({
    sourceFile: xlsNames[0]
});

console.log(xslAsJson);

app.get('/xslAsJson', function (req, res) {   
    var count = Object.keys(xslAsJson["ALTA ONE"]).length;
    console.log(count);
    res.send(xslAsJson);
    
});

app.listen(port, function (err) {
    console.log('Running server on port:' + port);
});
